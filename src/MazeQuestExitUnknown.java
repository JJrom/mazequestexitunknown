import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class MazeQuestExitUnknown {
    String[][] gameMaze = null;
    String[][] resultMaze = null;
    String fileUrl = "./src/maze-task-first.txt"; // Url to maze file
    String fileUrl2 = "./src/maze-task-second.txt"; // Url to maze file

    public static void main(String[] args) throws FileNotFoundException {

        MazeQuestExitUnknown mazeQuest = new MazeQuestExitUnknown();
        mazeQuest.solveMaze(mazeQuest.fileUrl);
        mazeQuest.solveMaze(mazeQuest.fileUrl2);

    }

    /**
     * Returns 2D Array from txt file of maze
     *
     * @param url URL to fetch file from
     * @return String[][] processed from txt file at URL
     * @throws FileNotFoundException if file does not exist
     */
    public String[][] fetchMaze(String url) throws FileNotFoundException {
        ArrayList<ArrayList<String>> mazeArrayList = new ArrayList<>(); // Using an arraylist for intermediary step, size of array is unknown
        File f = new File(url);
        Scanner sc = new Scanner(f);

        while (sc.hasNext()) {
            ArrayList<String> tmp = new ArrayList<>();
            String s = sc.nextLine();

            for (int i = 0; i < s.length(); i++) {
                String currentChar = Character.toString(s.charAt(i));
                tmp.add(currentChar);
            }
            mazeArrayList.add(tmp);
        }

        return mazeArrayList.stream().map(u -> u.toArray(new String[0])).toArray(String[][]::new); // Converts 2DArrayList to 2D Array
    }

    /**
     * Prints the String[][] representation of maze to terminal
     *
     * @param maze 2D String array
     */
    public void printMaze(String[][] maze) {
        System.out.println(Arrays.deepToString(maze).replace("], ", "]\n").replace("[[", "[").replace("]]", "]"));
        System.out.println();
    }

    /**
     * Checks legality of a move in given maze
     *
     * @param maze            String[][] representation of maze to check
     * @param targetPositionY Position to check on y axis
     * @param targetPositionX Position to check on x axis
     * @return True if move is legal (in bounds and either a space, or exit)
     */
    boolean isValidMove(String[][] maze, int targetPositionY, int targetPositionX) {

        if ((targetPositionX < 0 || targetPositionY < 0) || (targetPositionY >= maze.length || targetPositionX >= maze[targetPositionY].length)) { // out of bounds check
            return false;
        }

        return maze[targetPositionY][targetPositionX].equals(" ") || maze[targetPositionY][targetPositionX].equals("E");   // Space and exit represent legal moves

    }

    /**
     * Simple BFS to discover optimal solution (accepts the first optimal solution, ignores any equal length ones)
     *
     * @param maze           String[][] representation of the maze
     * @param startNode      Location coordinates for starting position
     * @param searchEndPoint Maximum depth of search
     * @return Length of the shortest path
     */
    public int findPathBfs(String[][] maze, Node startNode, int searchEndPoint) {

        boolean[][] visited = new boolean[maze.length][maze[0].length]; // Used to track visited spaces
        visited[startNode.locationY][startNode.locationX] = true;
        Queue<Node> queue = new LinkedList<>(); // Queue for bfs
        queue.add(startNode);

        while (!queue.isEmpty()) {
            Node currentNode = queue.poll();

            if (currentNode.distance > searchEndPoint) { // We did not find a path in time, ending search
                return Integer.MAX_VALUE;
            }

            if (maze[currentNode.locationY][currentNode.locationX].equals("E")) { // If exit point is reached, returning results
                int movesUsed = currentNode.distance; // Moves spent to reach exit
                resultMaze = gameMaze.clone();

                while (currentNode.parent != null) { // Reverse tracking path taken and marking it on the result array
                    resultMaze[currentNode.locationY][currentNode.locationX] = "P";
                    currentNode = currentNode.parent;
                }

                return movesUsed;
            }

            // Adding valid, unvisited locations to que
            if (isValidMove(maze, currentNode.locationY - 1, currentNode.locationX) && !visited[currentNode.locationY - 1][currentNode.locationX]) {
                visited[currentNode.locationY - 1][currentNode.locationX] = true;
                queue.add(new Node(currentNode.locationY - 1, currentNode.locationX, currentNode.distance + 1, currentNode));

            }

            if (isValidMove(maze, currentNode.locationY + 1, currentNode.locationX) && !visited[currentNode.locationY + 1][currentNode.locationX]) {
                visited[currentNode.locationY + 1][currentNode.locationX] = true;
                queue.add(new Node(currentNode.locationY + 1, currentNode.locationX, currentNode.distance + 1, currentNode));

            }
            if (isValidMove(maze, currentNode.locationY, currentNode.locationX + 1) && !visited[currentNode.locationY][currentNode.locationX + 1]) {
                visited[currentNode.locationY][currentNode.locationX + 1] = true;
                queue.add(new Node(currentNode.locationY, currentNode.locationX + 1, currentNode.distance + 1, currentNode));

            }
            if (isValidMove(maze, currentNode.locationY, currentNode.locationX - 1) && !visited[currentNode.locationY][currentNode.locationX - 1]) {
                visited[currentNode.locationY][currentNode.locationX - 1] = true;
                queue.add(new Node(currentNode.locationY, currentNode.locationX - 1, currentNode.distance + 1, currentNode));

            }

        }

        return Integer.MAX_VALUE; // No path found, returning Integer.MAX_VALUE

    }

    /**
     * Returns starting node in the maze
     *
     * @param maze String[][] representation of maze
     * @return Node for the starting position
     */
    Node getStartNode(String[][] maze) {

        for (int i = 0; i < maze.length; i++) {

            for (int n = 0; n < maze[i].length; n++) {

                if (maze[i][n].equals("^")) {
                    return new Node(i, n, 0, null);
                }
            }
        }
        return null;
    }

    /**
     * Solves and prints 2d maze from .txt file
     *
     * @param mazeUrl Path to maze .txt
     * @throws FileNotFoundException
     */
    void solveMaze(String mazeUrl) throws FileNotFoundException {
        gameMaze = fetchMaze(mazeUrl);
        resultMaze = gameMaze.clone();
        Node startingLocation = getStartNode(gameMaze);
        printMaze(gameMaze);  // Prints starting maze for reference

        if (startingLocation == null) {
            System.out.println("No starting point detected, terminating execution");

        } else {

            int pathLength = findPathBfs(gameMaze, startingLocation, 200); // Because bfs is used we only need to run once with 200 max depth, will find optimal path.

            printMaze(resultMaze);

            if (pathLength == Integer.MAX_VALUE) {
                System.out.println("Pentti is eternally doomed to stay in the maze");
            } else if (pathLength <= 20) {
                System.out.println("Pentti solves the maze with ease, only " + pathLength + " moves used");
            } else if (pathLength <= 150) {
                System.out.println("Pentti solves the maze with hard work, " + pathLength + " moves used");
            } else if (pathLength <= 200) {
                System.out.println("Pentti barely makes it, " + pathLength + " moves required");
            } else {
                System.out.println("Pentti could not make it, he would have needed " + pathLength + " moves");
            }
        }


    }

}
