public class Node {
    int distance, locationX, locationY;
    Node parent; // Used to reverse traverse path found via bfs

    /**
     * @param locationY location coordinate on y axis
     * @param locationX location coordinate on x axis
     * @param distance  distance from the first node
     * @param parent    parent of this node
     */
    public Node(int locationY, int locationX, int distance, Node parent) {
        this.locationY = locationY;
        this.locationX = locationX;
        this.distance = distance;
        this.parent = parent;
    }
}