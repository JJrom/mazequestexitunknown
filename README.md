#MazeQuestExitUnknown

This program finds optimal solution to 2D maze using bfs.

#Details
Input file should be a .txt file.
fileUrl in MazeQuestExitUnknown is used to determine what maze will be solved.

- "#" Denominates a wall in the maze
- " " Denominates open, traversable path
- "E" Denominates Exit from the maze. This program will find the optimal solution to the first viable exit. 
- "^" Denominates entry point for the maze. Will be automatically found from the maze file.

#Notes
Despite the request to run algorithm with 20, 150 and 200 moves, the fact that BFS was used means that one pass with 200 is enough. The program will alternate output based on wether it took <= 20, <=150 or <= 200 moves.
